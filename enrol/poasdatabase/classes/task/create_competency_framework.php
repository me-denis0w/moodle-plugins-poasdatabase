<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Create competency framework task
 * @package   enrol_poasdatabase
 * @copyright 2024 Sychev Oleg, Volgograd State Technical University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_poasdatabase\task;

defined('MOODLE_INTERNAL') || die();

/**
 * Class create_competency_framework
 * @package   enrol_poasdatabase
 * @copyright 2024 Sychev Oleg
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class create_competency_framework extends \core\task\scheduled_task {

    /**
     * Name for this task.
     *
     * @return string
     */
    public function get_name() {
        return get_string('createcompetencyframework', 'enrol_poasdatabase');
    }

    /**
     * Run task for creating competency framework.
     */
    public function execute() {
        $trace = new \text_progress_trace();

        if (!enrol_is_enabled('poasdatabase')) {
            $trace->output('Plugin not enabled');
            return;
        }

        $enrol = enrol_get_plugin('poasdatabase');

        $enrol->create_competency_framework($trace);
    }
}
