<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Task definition for enrol_poasdatabase.
 * @package   enrol_poasdatabase
 * @copyright 2023 Sychev Oleg
 * @copyright based on work by 2018 Daniel Neis Araujo <danielneis@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$tasks = array(
    array(
            'classname' => '\enrol_poasdatabase\task\sync_enrolments',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
    array(
            'classname' => '\enrol_poasdatabase\task\sync_cohorts2',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
    array(
            'classname' => '\enrol_poasdatabase\task\sync_courses_cohorts',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
    array(
            'classname' => '\enrol_poasdatabase\task\create_competency_framework',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
    array(
            'classname' => '\enrol_poasdatabase\task\sync_competencies',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
    array(
            'classname' => '\enrol_poasdatabase\task\sync_competencies_courses',
            'blocking' => 0,
            'minute' => 'R',
            'hour' => 'R',
            'day' => '*',
            'month' => '*',
            'dayofweek' => '*',
            'disabled' => 1
        ),
);