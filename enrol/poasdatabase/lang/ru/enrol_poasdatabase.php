<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_poasdatabase', language 'ru'.
 *
 * @package   enrol_poasdatabase
 * @copyright 2023 Sychev Oleg
 * @copyright based on work by 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allow_reenabling_teachers'] = 'Поддерживать преподавателей подписанными';
$string['allow_reenabling_teachers_desc'] = 'Если включено, то пользователи с ролью "editingteacher" (Преподаватель) будут иметь активную приписку на свои курсы, даже если информация о приписках больше не содержится во внешней базе данных. Действует для ночной синхронизации и при входе преподавателя на сайт. Эта настройка не касается ручного и других способов записи, которые не контролирует этот плагин.';
$string['allow_unenrol_2nd_teachers'] = 'Отписывать неактуальных преподавателей';
$string['allow_unenrol_2nd_teachers_desc'] = 'Если включено, то преподаватели, не являющиеся главными на своём курсе (по совпадению c idnumber преподавателя в курсе), будут заблокированы (при условии, что на курсе останется главный преподаватель). Эта настройка не касается ручного и других способов записи, которые не контролирует этот плагин.';
$string['course_idnumber_regex_capturing_teacher'] = 'Паттерн преподавателя в idnumber курса';
$string['course_idnumber_regex_capturing_teacher_desc'] = 'Если выполняется соглашение о том, что idnumber преподавателя включён в idnumber курса, то указание регулярного выражения в этом поле позволит преподавателям, являющиеся главными на своём курсе (по совпадению c idnumber преподавателя в курсе), будут активированы в любом случае (даже если они отсутствуют в таблице на зачисление). Регулярное выражение должно включать одну группу захвата (запоминающие скобки), которые должны заключать в себе idnumber преподавателя.';
$string['customsql'] = 'Выполнить SQL';
$string['customsql_desc'] = 'Запустить заданные SQL-команды на внешней БД в начале запланированной задачи sync_enrolments.';
$string['customsqlenable'] = 'Разрешить выполнение SQL';
$string['customsqlenable_desc'] = 'Разрешить запуск SQL-команд при каждом запуске запланированной задачи sync_enrolments.';
$string['poasdatabase:config'] = 'Настройка экземпляров плагина зачисления POAS database';
$string['poasdatabase:unenrol'] = 'Отчисление заблокированных пользователей';
$string['dbencoding'] = 'Кодировка базы данных';
$string['dbhost'] = 'Хост базы данных';
$string['dbhost_desc'] = 'Введите IP-адрес сервера базы данных или имя хоста. Используйте системное имя DSN, если используете ODBC. Используйте DSN PDO, если используете PDO.';
$string['dbname'] = 'Имя базы данных';
$string['dbname_desc'] = 'Оставьте пустым, если имя DSN присутствует в хосте базы данных.';
$string['dbpass'] = 'Пароль базы данных';
$string['dbsetupsql'] = 'Команда настройки базы данных';
$string['dbsetupsql_desc'] = 'SQL-команда для специальной настройки базы данных, часто используемая для настройки кодировки передачи данных - пример для MySQL и PostgreSQL: <em>SET NAMES \'utf8\'</em>';
$string['dbsybasequoting'] = 'Использовать кавычки как в sybase';
$string['dbsybasequoting_desc'] = 'Экранирование одинарных кавычек в стиле Sybase - необходимо для Oracle, MS SQL и некоторых других баз данных. Не используйте для MySQL!';
$string['dbtype'] = 'Драйвер базы данных';
$string['dbtype_desc'] = 'Имя драйвера базы данных ADOdb, тип внешнего компонента database engine.';
$string['dbuser'] = 'Пользователь базы данных';
$string['debugdb'] = 'Отладка ADOdb';
$string['debugdb_desc'] = 'Отладка подключения ADOdb к внешней базе данных - используется при появлении пустой страницы во время входа в систему. Не подходит для промышленной эксплуатации!';
$string['defaultcategory'] = 'Категория нового курса по умолчанию';
$string['defaultcategory_desc'] = 'Категория по умолчанию для автоматически создаваемых курсов. Используется, если идентификатор новой категории не указан или не найден.';
$string['defaultrole'] = 'Роль по умолчанию';
$string['defaultrole_desc'] = 'Роль, которая будет назначена по умолчанию, если во внешней таблице роль не указана.';
$string['extremovedcohortaction_desc'] = 'Выберите выполняемое действие при исчезновении глобальной группы из внешнего источника записи на курс. Обратите внимание, что вместе с отпиской глобальной группы от курса удаляются некоторые настройки и данные пользователей, состоявших в этой глобальной группе. На удаление глобальных групп влияет также следующая настройка.';
$string['extuserremovedfromcohortaction'] = 'Действие при исчезновении пользователя из глобальной группы во внешнем источнике';
$string['extuserremovedfromcohortaction_desc'] = 'Выберите выполняемое действие при исчезновении пользователя из состава глобальной группы во внешнем источнике. Обратите внимание, что при удалении студента из глобальной группы дальнейшие действия по блокировке либо полному отчислению из курсов выполняются плагином синхронизации глобальных групп (Плагины / Зачисления на курсы / Синхронизация с глобальной группой).';
$string['enableremovingduplicatecohorts'] = 'Включить удаление дубликатов глобальных групп, нештатно возникших при изменении name/idnumber во внешнем источнике';
$string['enableremovingduplicatecohorts_desc'] = 'Проверка и очистка при необходимости будет выполняться каждый раз при синхронизации глобальных групп (т.е. когорт).';
$string['enableupdatingcohorts'] = 'Включить обновление существующих глобальных групп при изменении во внешнем источнике';
$string['enableupdatingcohorts_desc'] = 'Обновление будет выполняться каждый раз при синхронизации глобальных групп (т.е. когорт).';
$string['enableupdatingcourses'] = 'Включить обновление существующих курсов при изменении во внешнем источнике';
$string['enableupdatingcourses_desc'] = 'Обновление будет выполняться каждый раз при синхронизации курсов.';
$string['updatecohortsmappingfield'] = 'Столбец для сопоставления существующих глобальных групп с внешним источником';
$string['updatecohortsmappingfield_desc'] = 'Внешние записи будут сопоставлены с существующими по этому полю (остальные поля могут изменяться).';
$string['updatecoursesmappingfield'] = 'Столбец для сопоставления существующих курсов с внешним источником';
$string['updatecoursesmappingfield_desc'] = 'Внешние записи будут сопоставлены с существующими по этому полю (остальные поля могут изменяться).';
$string['renameduplicatenames'] = 'Переименовывать курсы-дубликаты';
$string['renameduplicatenames_desc'] = 'Если эта опция включена, то при создании курсов с одинаковыми короткими именами (shortname), но разными idnumber, они будут переименованы автоматически добавлением « — N», чтобы обеспечить уникальность shortname.';
$string['ignorehiddencourses'] = 'Игнорировать скрытые курсы';
$string['ignorehiddencourses_desc'] = 'Если эта опция включена, пользователи не будут зачислены на курсы, которые настроены как недоступные для студентов.';
$string['keepenrolledcohorts'] = 'Сохранять глобальные группы, имеющие запись на курс';
$string['keepenrolledcohorts_desc'] = 'Эта настройка используется, если выбран режим удаления глобальных групп при исчезновении их во внешнем источнике. Если включено, то глобальные группы, приписанные хотя бы на один курс, не будут удалены.';
$string['laggingstudentstable'] = 'Внешняя таблица отстающих студентов';
$string['laggingstudentstable_desc'] = 'Отстающими студентами считаются те, кто не закрыл предмет в течение семестра и потерял доступ к курсу вместе со своей группой, но кому ещё требуется доступ для просмотра материалов и/или размещения ответов на задания. Оставьте имя таблицы пустым, чтобы не использовать эту возможность.';
$string['laggingstudentlogin'] = 'Столбец с логином студента';
$string['laggingstudentlogin_desc'] = 'Студент, который должен быть повторно зачислен на курс, определяется по логину, задаётся логином (именем пользователя), который сохранён в этом столбце внешней таблицы.';
$string['laggingexactcourseidnumber'] = 'Столбец с idnumber курса';
$string['laggingexactcourseidnumber_desc'] = 'Если курс, на который нужно записать студента, не известен в точности, то это поле должно быть оставлено пустым; иначе должно содержать точный idnumber курса, тогда шаблон (ниже) игнорируется.';
$string['laggingcourseidnumberpattern'] = 'Столбец с шаблоном для idnumber курса';
$string['laggingcourseidnumberpattern_desc'] = 'Если точный idnumber курса неизвестен (предыдущее поле оказалось пустым), тогда это поле должно содержать шаблон для idnumber курса в нотации SQL-LIKE (где "%" обозначает произвольный текст).';
$string['localcategoryfield'] = 'Локальное поле категории';
$string['localcoursefield'] = 'Локальное поле курса';
$string['localrolefield'] = 'Локальное поле роли';
$string['localuserfield'] = 'Локальное поле пользователя';
$string['newcoursetable'] = 'Внешняя таблица курсов';
$string['newcoursetable_desc'] = 'Укажите название таблицы, содержащей список курсов, которые должны быть созданы автоматически. Пустое значение означает, что курсы не будут созданы.';
$string['newcoursecategory'] = 'Поле категории нового курса';
$string['newcoursefullname'] = 'Поле полного имени нового курса';
$string['newcourseidnumber'] = 'Поле idnumber нового курса';
$string['newcourseshortname'] = 'Поле краткого имени нового курса';
$string['newcompetencytable'] = 'Внешняя таблица компетенций';
$string['newcompetencytable_desc'] = 'Укажите название таблицы, содержащей список компетенций, которые должны быть созданы автоматически. Пустое значение означает, что компетенции не будут созданы.';
$string['newcompetencyshortname'] = 'Поле краткого имени новой компетенции';
$string['newcompetencydescription'] = 'Поле описания новой компетенции';
$string['newcompetencyidnumber'] = 'Поле idnumber новой компетенции';
$string['coursescompetenciestable'] = 'Внешняя таблица связи курсов-компетенций';
$string['coursescompetenciestable_desc'] = 'Укажите название таблицы, содержащей список взаимосвязей курсов и компетенций, которые должны быть созданы автоматически. Пустое значение означает, что компетенции не будут привязываться.';
$string['coursescompetenciesidnumberframework'] = 'Поле idnumber фреймворка компетенций';
$string['coursescompetenciesnameframework'] = 'Поле имя фреймворка компетенций';
$string['coursescompetenciesidnumbercompetency'] = 'Поле idnumber компетенции';
$string['coursescompetenciesnamecompetency'] = 'Поле имя компетенции';
$string['coursescompetenciesdesccompetency'] = 'Поле описание компетенции';
$string['coursescompetenciesidnumbercourse'] = 'Поле idnumber курса';
$string['extcompetencyremoved'] = 'Действие при исчезновении компетенции во внешнем источнике';
$string['extcompetencyremoved_desc'] = 'Выберите действие, которое необходимо выполнить при исчезновении компетенции во внешней базе данных.';
$string['option_extcompetencyremovednothing'] = 'Сохранять компетенции';
$string['option_extcompetencyremovedremove'] = 'Удалить компетенции';
$string['option_cohortextremovedkeep'] = 'Оставить глобальную группу записанной на курс';
$string['option_cohortextremovedsuspend'] = 'Приостановить подписку на курс';
$string['option_cohortextremovedunenrol'] = 'Отписать от всех курсов и удалить глобальную группу';
$string['option_usercohortextremovedkeep'] = 'Оставить в глобальной группе (записанным)';
$string['option_usercohortextremovedsuspend'] = 'Заблокировать на всех курсах (переопределяется настройкой плагина глоб. групп)';
$string['option_usercohortextremovedunenrol'] = 'Удалить из глобальной группы';
$string['pluginname'] = 'Внешняя база данных (ПОАС)';
$string['pluginname_desc'] = 'Вы можете использовать внешнюю базу данных (практически любого типа) для контроля зачислений. Предполагается, что ваша внешняя база данных содержит, по крайней мере, поле, содержащее идентификатор курса, и поле, содержащее идентификатор пользователя. Они сравниваются с полями, которые вы выбираете в локальных таблицах курсов и пользователей. Плагин усовершенствован кафедрой программного обеспечения автоматизированных систем (ПОАС) Волгоградского государственного технического университета.';
$string['remotecategoriestable'] = 'Внешняя таблица категорий';
$string['remotecategoriestable_desc'] = 'Укажите имя таблицы, содержащей перечень категорий, которые должны быть автоматически созданы. При пустом значении категории не создаются.';
$string['remotecategorynamefield'] = 'Поле полного имени новой категории';
$string['remotecategoryidnumberfield'] = 'Поле номера-ID новой категории';
$string['remotecohortdescriptionfield'] = 'Поле описания новой глобальной группы';
$string['remotecohortidnumberfield'] = 'Поле номера-ID новой глобальной группы';
$string['remotecohortnamefield'] = 'Поле полного имени новой глобальной группы';
$string['remotecohortmemberrole'] = 'Внешнее поле роли группы';
$string['remotecohortmemberrole_desc'] = 'Поле роли для пользователей глобальной группы при записи их на курс — имя поля во внешней таблице. Значения используются для сопоставления записей в таблице ролей (по именам ролей).';
$string['remotecohortstable'] = 'Внешняя таблица глобальных групп';
$string['remotecohortstable_desc'] = 'Укажите имя таблицы, содержащей перечень когорт, которые должны быть автоматически созданы. При пустом значении когорты не создаются.';
$string['remotecoursecohorttable'] = 'Внешняя таблица когорт и курсов';
$string['remotecoursecohorttable_desc'] = 'Укажите имя таблицы, содержащей перечень связей между глобальными группами и курсами для зачисления.';
$string['remotecoursefield'] = 'Внешнее поле курса';
$string['remotecoursefield_desc'] = 'Имя поля во внешней таблице, которое используется для сопоставления записей в таблице курсов.';
$string['remotecourseidnumberfield'] = 'Поле idnumber курса';
$string['remoteenroltable'] = 'Внешняя таблица зачисления пользователей';
$string['remoteenroltable_desc'] = 'Укажите имя таблицы, содержащей список пользователей к зачислению. Пустое значение означает отсутствие синхронизации зачислений пользователей.';
$string['remotegroupnamefield'] = 'Поле наименования группы';
$string['remoteotheruserfield'] = 'Внешнее поле отметки "Другой пользователь"';
$string['remoteotheruserfield_desc'] = 'Имя поля во внешней таблице, которое используется для отметки назначений с ролью "Другой пользователь".';
$string['remoterolefield'] = 'Внешнее поле роли';
$string['remoterolefield_desc'] = 'Имя поля в удаленной таблице, которое мы используем для сопоставления записей в таблице ролей.';
$string['remotetagtable'] = 'Таблица тегов';
$string['remotetagcourseidnumberfield'] = 'Поле idnumber курса в таблице тегов';
$string['remotetagtagnamefield'] = 'Поле имени тега в таблице тегов';
$string['remoteusercohorttable'] = 'Внешняя таблица пользователей и когорт';
$string['remoteusercohorttable_desc'] = 'Укажите имя таблицы, содержащей перечень связей пользователей и когорт, которые должны быть автоматически созданы. При пустом значении связи не создаются.';
$string['remoteusercohortcohortidnumberfield'] = 'Поле idnumber глобальной группы в таблице';
$string['remoteusercohortuserloginfield'] = 'Поле логина пользователя';
$string['remoteuserfield'] = 'Внешнее поле пользователя';
$string['settingsheadercoursecohort'] = 'Зачисление на курсы на основании глобальной группы';
$string['settingsheaderdb'] = 'Подключение к внешней базе данных';
$string['settingsheaderlaggingstudents'] = 'Зачисление отстающих студентов';
$string['settingsheaderlocal'] = 'Сопоставление локальных полей';
$string['settingsheaderremote'] = 'Синхронизация индивидуального зачисления на курс';
$string['settingsheadernewcategories'] = 'Создание категорий';
$string['settingsheadernewcohorts'] = 'Создание глобальных групп';
$string['settingsheadernewcourses'] = 'Создание курсов';
$string['settingsheaderusercohort'] = 'Распределение пользователей по глобальным группам';
$string['settingsheadernewcompetencies'] = 'Создание компетенций';
$string['settingsheadercoursescompetencies'] = 'Синхронизация курсов и компетенций';
$string['settingsheadernewcompetencies'] = 'Создание компетенций';
$string['settingsheadercoursescompetencies'] = 'Синхронизация курсов и компетенций';
$string['synccohortstask'] = 'Задача синхронизации когорт из внешней базы данных';
$string['synccohortcoursesstask'] = 'Задача синхронизации когорт внешней базы данных с группами в курсах';
$string['createcompetencyframework'] = 'Задача создания фреймворков компетенций';
$string['synccompetencies'] = 'Задача синхронизации компетенций из внешней базы данных';
$string['synccompetenciescourses'] = 'Задача синхронизации компетенций внешней базы данных с курсами';
$string['createcompetencyframework'] = 'Задача создания фреймворков компетенций';
$string['synccompetencies'] = 'Задача синхронизации компетенций из внешней базы данных';
$string['synccompetenciescourses'] = 'Задача синхронизации компетенций внешней базы данных с курсами';
$string['syncenrolmentstask'] = 'Задача синхронизации зачислений из внешней базы данных';
$string['synclaggingstudents'] = 'Зачисление отстающих студентов на те курсы, которые им (возможно) необходимы';
$string['remoteuserfield_desc'] = 'Имя поля во внешней таблице, которое используется для сопоставления записей в таблице пользователей.';
$string['templatecourse'] = 'Шаблон нового курса';
$string['templatecourse_desc'] = 'Необязательно: автоматически создаваемые курсы могут копировать свои настройки из шаблонного курса. Введите здесь краткое название (shortname) шаблонного курса.';
$string['privacy:metadata'] = 'Плагин External database enrolment не хранит никаких персональных данных.';
