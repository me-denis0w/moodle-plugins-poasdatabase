<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_poasdatabase', language 'en'.
 *
 * @package   enrol_poasdatabase
 * @copyright 2023 Sychev Oleg
 * @copyright based on work by 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allow_reenabling_teachers'] = 'Keep teachers enrolled';
$string['allow_reenabling_teachers_desc'] = 'If enabled, users with the "editingteacher" role will have active enrollment in their courses, even if the enrollment information is no longer in the external database. Applies to nightly sync and when teachers log in to the site. This setting does not apply to manual and other methods of enrollment that this plugin does not manage.';
$string['allow_unenrol_2nd_teachers'] = 'Unenroll irrelevant teachers';
$string['allow_unenrol_2nd_teachers_desc'] = 'If enabled, teachers who are not in the lead on their course (by matching the teacher idnumber in the course) will be blocked (as long as the lead teacher remains on the course). This setting does not apply to manual and other enrollment methods that this plugin does not manage.';
$string['course_idnumber_regex_capturing_teacher'] = 'Lecturer regex-pattern in the course idnumber';
$string['course_idnumber_regex_capturing_teacher_desc'] = 'If there is a convention that the instructor idnumber is included in the course idnumber, then specifying a regular expression in this field will allow instructors who are head in their course (by matching the instructor idnumber in the course) to be activated in any case (even if they are not in the enrollment table). The regular expression must include one capture group (parentheses), which must contain the instructor\'s idnumber.';
$string['customsql'] = 'Custom SQL';
$string['customsql_desc'] = 'Run custom SQL on the external DB at the start of sync_enrolments scheduled task.';
$string['customsqlenable'] = 'Enable custom SQL';
$string['customsqlenable_desc'] = 'Enable running custom SQL each time sync_enrolments scheduled task is run.';
$string['poasdatabase:config'] = 'Configure POAS database enrol instances';
$string['poasdatabase:unenrol'] = 'Unenrol suspended users';
$string['dbencoding'] = 'Database encoding';
$string['dbhost'] = 'Database host';
$string['dbhost_desc'] = 'Type database server IP address or host name. Use a system DSN name if using ODBC. Use a PDO DSN if using PDO.';
$string['dbname'] = 'Database name';
$string['dbname_desc'] = 'Leave empty if using a DSN name in database host.';
$string['dbpass'] = 'Database password';
$string['dbsetupsql'] = 'Database setup command';
$string['dbsetupsql_desc'] = 'SQL command for special database setup, often used to setup communication encoding - example for MySQL and PostgreSQL: <em>SET NAMES \'utf8\'</em>';
$string['dbsybasequoting'] = 'Use sybase quotes';
$string['dbsybasequoting_desc'] = 'Sybase style single quote escaping - needed for Oracle, MS SQL and some other databases. Do not use for MySQL!';
$string['dbtype'] = 'Database driver';
$string['dbtype_desc'] = 'ADOdb database driver name, type of the external database engine.';
$string['dbuser'] = 'Database user';
$string['debugdb'] = 'Debug ADOdb';
$string['debugdb_desc'] = 'Debug ADOdb connection to external database - use when getting empty page during login. Not suitable for production sites!';
$string['defaultcategory'] = 'Default new course category';
$string['defaultcategory_desc'] = 'The default category for auto-created courses. Used when no new category id specified or not found.';
$string['defaultrole'] = 'Default role';
$string['defaultrole_desc'] = 'The role that will be assigned by default if no role is specified in external table.';
$string['extremovedcohortaction_desc'] = 'Select the action to be performed when the cohort disappears from the external database. Please note that  unsubscription of a cohort causes removal from the course of some settings and data of users who were in this cohort. Cohort removal is also affected by the setting below.';
$string['extuserremovedfromcohortaction'] = 'Action when user removed from cohort in the external database';
$string['extuserremovedfromcohortaction_desc'] = 'Select the action to be performed when the user disappears from a cohort in the external database. Please note that disabling or unenrolment of a student is managed by cohort sync plugin (Plugins / Enrolments / Cohort sync).';
$string['enableremovingduplicatecohorts'] = 'Enable deletion of duplicate cohorts abnormally generated due to name/idnumber changes in the external source';
$string['enableremovingduplicatecohorts_desc'] = 'Verification and cleanup if necessary will be performed each time the cohorts are synchronised.';
$string['enableupdatingcohorts'] = 'If enabled, existing cohorts will be updated if changed in the external source';
$string['enableupdatingcohorts_desc'] = 'The update will be performed each time on cohorts synchronization.';
$string['enableupdatingcourses'] = 'If enabled, existing courses will be updated if changed in the external source';
$string['enableupdatingcourses_desc'] = 'The update will be performed each time on courses synchronization.';
$string['updatecohortsmappingfield'] = 'Column to match external cohorts with existing ones';
$string['updatecohortsmappingfield_desc'] = 'Existing and external records will be mapped by this column, so the other columns may change.';
$string['updatecoursesmappingfield'] = 'Column to match external courses with existing ones';
$string['updatecoursesmappingfield_desc'] = 'Existing and external records will be mapped by this column, so the other columns may change.';
$string['renameduplicatenames'] = 'Rename duplicate courses';
$string['renameduplicatenames_desc'] = 'If this option is enabled, when creating courses, ones with the same `shortname`s but different `idnumber`s will be renamed automatically by adding " — N" to ensure the uniqueness of the shortname.';
$string['ignorehiddencourses'] = 'Ignore hidden courses';
$string['ignorehiddencourses_desc'] = 'If enabled users will not be enrolled on courses that are set to be unavailable to students.';
$string['keepenrolledcohorts'] = 'Keep cohorts having any enrolment';
$string['keepenrolledcohorts_desc'] = 'This setting is used if cohort deletion mode is selected when they disappear from an external source. If enabled, cohorts enrolled in at least one course will not be deleted.';
$string['laggingstudentstable'] = 'Lagging students table';
$string['laggingstudentstable_desc'] = 'Lagging students are students that haven`t completed a course which has finished and still need access to it (to view the material and/or submit answers to assignments) but were automatically unenrolled with their group. Leave the table name empty to turn the feature off.';
$string['laggingstudentlogin'] = 'Column to match student\'s user login';
$string['laggingstudentlogin_desc'] = 'Student`s user login column name in the external database.';
$string['laggingexactcourseidnumber'] = 'Column to match course idnumber';
$string['laggingexactcourseidnumber_desc'] = 'If the student\'s course is not known exactly, this field in a such row should be empty, otherwise the exact matching is done only and the pattern (see below) is ignored.';
$string['laggingcourseidnumberpattern'] = 'Column to match course idnumber by pattern';
$string['laggingcourseidnumberpattern_desc'] = 'If exact course idnumber (the previous field) is empty, this field should contain an pattern of course idnumber in SQL-LIKE notation (using percent char "%" in place of arbitrary text).';
$string['localcategoryfield'] = 'Local category field';
$string['localcoursefield'] = 'Local course field';
$string['localrolefield'] = 'Local role field';
$string['localuserfield'] = 'Local user field';
$string['newcoursetable'] = 'Remote new courses table';
$string['newcoursetable_desc'] = 'Specify of the name of the table that contains list of courses that should be created automatically. Empty means no courses are created.';
$string['newcoursecategory'] = 'New course category field';
$string['newcoursefullname'] = 'New course full name field';
$string['newcourseidnumber'] = 'New course ID number field';
$string['newcourseshortname'] = 'New course short name field';
$string['newcompetencytable'] = 'Remote new competencies table';
$string['newcompetencytable_desc'] = 'Specify of the name of the table that contains list of competencies that should be created automatically. Empty means no competencies are created.';
$string['newcompetencyshortname'] = 'New competency short name field';
$string['newcompetencydescription'] = 'New competency description field';
$string['newcompetencyidnumber'] = 'New competency ID number field';
$string['coursescompetenciestable'] = 'Remote courses-competencies relationships table';
$string['coursescompetenciestable_desc'] = 'Specify of the name of the table that contains list of course and competency relationships that should be created automatically. Empty means no competencies are created';
$string['coursescompetenciesidnumberframework'] = 'idnumber competency framework field';
$string['coursescompetenciesnameframework'] = 'name competency framework field';
$string['coursescompetenciesidnumbercompetency'] = 'idnumber competency field';
$string['coursescompetenciesnamecompetency'] = 'name competency field';
$string['coursescompetenciesdesccompetency'] = 'description competency field';
$string['coursescompetenciesidnumbercourse'] = 'idnumber course field';
$string['extcompetencyremoved'] = 'Action when competency removed in the external database';
$string['extcompetencyremoved_desc'] = 'Select the action to be performed when the competency disappears in the external database.';
$string['option_extcompetencyremovednothing'] = 'Keep competencies';
$string['option_extcompetencyremovedremove'] = 'Remove from competencies';
$string['option_cohortextremovedkeep'] = 'Keep cohort enrolled';
$string['option_cohortextremovedsuspend'] = 'Suspend course enrolment (keep user data)';
$string['option_cohortextremovedunenrol'] = 'Remove cohort & unenroll from courses completely';
$string['option_usercohortextremovedkeep'] = 'Keep in cohort (keep enrolled)';
$string['option_usercohortextremovedsuspend'] = 'Suspend on all courses (refedined by cohort enrollment plugin)';
$string['option_usercohortextremovedunenrol'] = 'Remove from cohort';
$string['pluginname'] = 'POAS External database';
$string['pluginname_desc'] = 'You can use an external database (of nearly any kind) to control your enrolments. It is assumed your external database contains at least a field containing a course ID, and a field containing a user ID. These are compared against fields that you choose in the local course and user tables. Enhanced by Software Engineering (POAS) Department of Volgograd State Technical University.';
$string['remotecategoriestable'] = 'Remote categories table';
$string['remotecategoriestable_desc'] = 'Specify of the name of the table that contains list of categories that should be created automatically. Empty means no categories are created.';
$string['remotecategorynamefield'] = 'Remote category full name field';
$string['remotecategoryidnumberfield'] = 'Remote category idnumber field';
$string['remotecohortdescriptionfield'] = 'Remote cohort description field';
$string['remotecohortidnumberfield'] = 'Remote cohort idnumber field';
$string['remotecohortnamefield'] = 'Remote cohort full name field';
$string['remotecohortmemberrole'] = 'Remote cohort role field';
$string['remotecohortmemberrole_desc'] = 'The role for the cohort when enrolling in a course: the name of the field in remote table. Values are used to compare records in the role table (by role name).';
$string['remotecohortstable'] = 'Remote cohorts table';
$string['remotecohortstable_desc'] = 'Specify of the name of the table that contains list of cohorts that should be created automatically. Empty means no cohorts are created.';
$string['remotecoursecohorttable'] = 'Remote cohort-course mapping table';
$string['remotecoursecohorttable_desc'] = 'Specify of the name of the table that contains list of cohort-course relations that ulitized for automatic enrolment.';
$string['remotecoursefield'] = 'Remote course field';
$string['remotecoursefield_desc'] = 'The name of the field in the remote table that we are using to match entries in the course table.';
$string['remotecourseidnumberfield'] = 'Remote course idnumber field';
$string['remoteenroltable'] = 'Remote user enrolment table';
$string['remoteenroltable_desc'] = 'Specify the name of the table that contains list of user enrolments. Empty means no user enrolment sync.';
$string['remotegroupnamefield'] = 'Remote group name field';
$string['remoteotheruserfield'] = 'Remote Other User field';
$string['remoteotheruserfield_desc'] = 'The name of the field in the remote table that we are using to flag "Other User" role assignments.';
$string['remoterolefield'] = 'Remote role field';
$string['remoterolefield_desc'] = 'The name of the field in the remote table that we are using to match entries in the roles table.';
$string['remotetagtable'] = 'Remote tag table';
$string['remotetagcourseidnumberfield'] = 'Course idnumber field in tag table';
$string['remotetagtagnamefield'] = 'Tag name field in tag table';
$string['remoteusercohorttable'] = 'Remote user-cohort mapping table';
$string['remoteusercohorttable_desc'] = 'Specify the name of the table that contains list of user-cohort relations. Empty means no user-cohort sync.';
$string['remoteusercohortcohortidnumberfield'] = 'Cohort idnumber field in the table';
$string['remoteusercohortuserloginfield'] = 'User login field in the table';
$string['remoteuserfield'] = 'Remote user field';
$string['settingsheadercoursecohort'] = 'Cohort-based enrolment to courses';
$string['settingsheaderdb'] = 'External database connection';
$string['settingsheaderlaggingstudents'] = 'Enrolling lagging students';
$string['settingsheaderlocal'] = 'Local field mapping';
$string['settingsheaderremote'] = 'Remote enrolment sync';
$string['settingsheadernewcategories'] = 'Creation of new categories';
$string['settingsheadernewcohorts'] = 'Creation of new cohorts';
$string['settingsheadernewcourses'] = 'Creation of new courses';
$string['settingsheaderusercohort'] = 'Adding users to cohorts';
$string['settingsheadernewcompetencies'] = 'Creation of new competencies';
$string['settingsheadercoursescompetencies'] = 'Courses and competencies synchronization';
$string['settingsheadernewcompetencies'] = 'Creation of new competencies';
$string['settingsheadercoursescompetencies'] = 'Courses and competencies synchronization';
$string['synccohortstask'] = 'Synchronise external database cohorts task';
$string['synccohortcoursesstask'] = 'Synchronise external database cohorts groups in courses task';
$string['createcompetencyframework'] = 'Create competency frameworks task';
$string['synccompetencies'] = 'Create competencies task';
$string['synccompetenciescourses'] = 'Synchronise external database competencies in courses task';
$string['createcompetencyframework'] = 'Create competency frameworks task';
$string['synccompetencies'] = 'Create competencies task';
$string['synccompetenciescourses'] = 'Synchronise external database competencies in courses task';
$string['syncenrolmentstask'] = 'Synchronise external database enrolments task';
$string['synclaggingstudents'] = 'Enrol lagging students back to courses they (may) need';
$string['remoteuserfield_desc'] = 'The name of the field in the remote table that we are using to match entries in the user table.';
$string['templatecourse'] = 'New course template';
$string['templatecourse_desc'] = 'Optional: auto-created courses can copy their settings from a template course. Type here the shortname of the template course.';
$string['privacy:metadata'] = 'The External database enrolment plugin does not store any personal data.';
