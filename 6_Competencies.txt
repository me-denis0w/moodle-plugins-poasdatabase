

# ФУНКЦИЯ Обрезки длинного названия (по пробелу)
# Если длина строки s превышает maxlen, то строка обрезается по ровному слову, и в конце вставляется символ многоточия.
# Если в результате обрезки по пробелу получилась пустая строка, то обрезка будет выполнена по символам.
DROP FUNCTION IF EXISTS RTRIM_TITLE;
DELIMITER $$
CREATE FUNCTION RTRIM_TITLE (s VARCHAR(255), maxlen INT)
RETURNS VARCHAR(255)
DETERMINISTIC
BEGIN
  DECLARE res VARCHAR(255);
  declare i int;

  if CHAR_LENGTH(s) <= maxlen then
	RETURN s;
  end if;

  set res = CONCAT(SUBSTRING(s, 1, maxlen - 1), "…");
  set i = maxlen;
  while i > 0 do
	if SUBSTRING(s, i, 1) = " " OR SUBSTRING(s, i, 1) = "(" then
		set res = CONCAT(SUBSTRING(s, 1, i - 1), "…");
		set i = 0;  # break
	end if;
	set i = i - 1;
  end while;

  RETURN res;
END$$
DELIMITER ;


# Тесты для функции
-- SELECT RTRIM_TITLE("You should have received a copy of the GNU General Public License", 13) as trimmed;  # You should…
-- SELECT trimmed, CHAR_LENGTH(trimmed) FROM
-- (SELECT RTRIM_TITLE("что-то из загружаемого контента видно всем пользователям, что мотивирует загружать корректную информацию", 80) as trimmed) t  # что-то из загружаемого контента видно всем пользователям, что мотивирует…


DROP VIEW IF EXISTS view_competencies_courses;

CREATE VIEW view_competencies_courses
AS SELECT DISTINCT 
	x.syllabus_curriculum_code as id_number_framework, 
    x.syllabus_competence_abbr as shortname_competency, 
	CONCAT(y.syllabus_faculty_quick_entry_number, '+', y.syllabus_specialty_code, '-', x.syllabus_discipline_code, '*%') as id_number_course, 
    CONCAT(x.syllabus_curriculum_code, '+', x.syllabus_competence_code, '/', x.syllabus_competence_abbr) as id_number_competency, 
    CONCAT(x.syllabus_competence_abbr, ' ', x.syllabus_competence) as description_competency, 
    CONCAT(
        y.syllabus_specialty_code, ' ', 
        y.syllabus_qualification, ' — ',
        RTRIM_TITLE(y.syllabus_profile, 100 - 8 - 2 - CHAR_LENGTH(y.syllabus_specialty_code) - CHAR_LENGTH(y.syllabus_qualification) - CHAR_LENGTH(y.syllabus_edu_type_name)), ' ',  # Long content will be trimmed to fit 100 chars.
        y.syllabus_edu_type_name, ' ', 
        LEFT(y.syllabus_form_of_training, 1), '_', 
        LEFT(y.syllabus_is_reduced_training_time, 1)
	) AS name_framework 
FROM curriculum_competence_raw_table x 
JOIN syllabi_raw_table y ON x.syllabus_curriculum_code = y.syllabus_curriculum_foundation;
