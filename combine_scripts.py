# combine_scripts.py

"""
A Python 3 script designed to strip all comments from four SQL scripts and merge them into one.
The combined script can be used in a production environment to execute all SQL commands at once, instead of loading and executing each of the scripts manually.
This script is expected to be run whenever a change in functionality is introduced in any of the scripts involved.

No third-party Python libraries required.
Tested with Python 3.8.
"""

import re
import os.path

# configuration

src_folder = '.'  # where to read from
dst_folder = '.'  # where to write to

script_names = [
    '1_Users.txt',
    '2_Courses.txt',
    '3_Enrolments.txt',
    '4_marks.txt',
    '6_Competencies.txt',
    '7_Curriculum.txt',
    '8_Defer_removal.txt',
]

combined_script_name = 'Scripts_1-8_%s.sql'

version_string = None  # use current date
# version_string = '2023-11-13'  # use specific date as version

line_comment_mark = r'(?:#|-- |/\*[\S\s]*?\*/)'


# code

MUTLIPLE_NL_re = re.compile(r'\n{3,}')

def remove_extra_empty_lines(text):
    return MUTLIPLE_NL_re.sub(r'\n\n', text)

LINE_COMMENT_re = re.compile(r'^\s*%s.*(?:\n|$)' % line_comment_mark, re.M)

def remove_comments(text):
    if line_comment_mark:
        return LINE_COMMENT_re.sub('', text)
    return text

def file_content(path, basedir=None):
    """Read content of file under given path, process it and return.
    Processing steps:
    1) remove all comments;
    2) remove extra empty lines.
    """
    try:
        with open(path, encoding='utf8') as f:
            text = f.read()
            text = text.strip()
            text = remove_comments(text)
            text = remove_extra_empty_lines(text)
            text = text.strip()
            return text
    except Exception as e:
        if basedir:
            return file_content(os.path.join(basedir, path), basedir=None)
        print(type(e), e)
        print(f'>>>>>>>>ERROR reading file: "{path}"')


def current_date():
    """Returns formatted date, ex. '2023-11-13' (YYYY-mm-dd)."""
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d')
    return formatted_time


def main():
    """Create a merged script using variables defined under `configuration` comment above."""
    date_string = current_date()
    version = version_string or date_string
    full_text = ''

    for file_name in script_names:
        full_text += f'##### {file_name} #####\n\n'
        full_text += file_content(file_name, src_folder)
        full_text += '\n\n'

    full_text += f'# ------ Version: {version}\n'
    full_text += f'# ----- Saved at: {date_string}\n'

    combined_script_path = os.path.join(dst_folder, combined_script_name % version)
    with open(combined_script_path, 'w') as f:
        f.write(full_text)
    print('saved:', combined_script_path)


if __name__ == '__main__':
    main()

