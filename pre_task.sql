-- pre_task.sql
-- Изменено: 10.02.2025.

-- зачисление на МООКи:
-- 	→ преподавателей ассистентами (или, для начала, студентами?)
-- 	→ задолжников студентами (вне групп)
-- 	→ глоб.групп (когорт) студентов, которые сейчас учатся соответствующей дисциплине

-- Коды дисциплин:
-- "Основы российской государственности" ('ОРГ'): '7832     '
-- "История": '000000005', "История России": '000003803', "История (История России, всеобщая история)": '000005325'
-- "Физика":  '(000000008|000000943)' — Физика, Физика (специальные главы).

-- idnumber целевых курсов (МООК-ов):
-- mooc_history1
-- mooc_history2
-- mooc_org1
-- mooc_physics1
-- (тайм-менеджмент??)

-- Паттерны для фильтрации по факультетам студентов (и, таким образом, по филиалам):
-- ВолгГТУ    (10[1-7]|11[37])\\+
-- ИАиС       (10[89]|11[05])\\+
-- ВПИ        20[012]\\+
-- КТИ        30[23]\\+

-- dev tip: do NOT use semicolon within comments here!!! This breaks splitting of this text and leads to SQL syntax errors!


DROP TABLE IF EXISTS `mooc_members`;

CREATE TABLE IF NOT EXISTS `mooc_members` (
  `course_idnumber` varchar(99) NOT NULL DEFAULT '',
  `member_login` varchar(255) NOT NULL,
  `member_role` varchar(31) NOT NULL DEFAULT ''
);



# Преподаватели и задолжники на курсах (1сем) в филиалах:
# + на 106 — ФЭВТ, 3 курс (один курс у ИВТ на 5 семестре)

-- дисциплина ОРГ: '7832     '

# ВолГТУ → МООК ОРГ (1сем)
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-7832     \\*[15]' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-7832     \\*[15]' AND member_role = 'student';
;

# ИАиС → МООК ОРГ (1сем)
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-7832     \\*1' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-7832     \\*1' AND member_role = 'student';
;

# ВПИ → МООК ОРГ (1сем)
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '20[012]\\+.*-7832     \\*1' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '20[012]\\+.*-7832     \\*1' AND member_role = 'student';
;

# КТИ → МООК ОРГ (1сем)
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '30[23]\\+.*-7832     \\*1' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_org1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '30[23]\\+.*-7832     \\*1' AND member_role = 'student';
;




-- дисциплина Физика: '(000000008|000000943)'

# ВолГТУ → МООК Физика (1сем)
insert into mooc_members
select distinct 'mooc_physics1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_physics1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
;

# ИАиС → МООК Физика (1сем)
insert into mooc_members
select distinct 'mooc_physics1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_physics1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
;

-- # ВПИ → МООК Физика (1сем)
-- insert into mooc_members
-- select distinct 'mooc_physics1' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_physics1' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
-- ;

-- # КТИ → МООК Физика (1сем)
-- insert into mooc_members
-- select distinct 'mooc_physics1' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_physics1' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
-- ;




-- дисциплина История: '000000005'
-- "История": '000000005'
-- "История России": '000003803'
-- "История (История России, всеобщая история)": '000005325'
-- (000000005|000003803|000005325)


# ВолГТУ → МООК История (1сем)
insert into mooc_members
select distinct 'mooc_history1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_history1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
;

# ИАиС → МООК История (1сем)
insert into mooc_members
select distinct 'mooc_history1' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_history1' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
;

-- # ВПИ → МООК История (1сем)
-- insert into mooc_members
-- select distinct 'mooc_history1' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_history1' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
-- ;

-- # КТИ → МООК История (1сем)
-- insert into mooc_members
-- select distinct 'mooc_history1' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_history1' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
-- ;





# ВолГТУ → МООК История (2сем)
insert into mooc_members
select distinct 'mooc_history2' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_history2' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
;

# ИАиС → МООК История (2сем)
insert into mooc_members
select distinct 'mooc_history2' as course_idnumber, member_login, 'teacher' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'editingteacher';
;
insert into mooc_members
select distinct 'mooc_history2' as course_idnumber, member_login, 'student' as member_role
from courses_members
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
;

-- # ВПИ → МООК История (2сем)
-- insert into mooc_members
-- select distinct 'mooc_history2' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_history2' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '20[012]\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
-- ;

-- # КТИ → МООК История (2сем)
-- insert into mooc_members
-- select distinct 'mooc_history2' as course_idnumber, member_login, 'teacher' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'editingteacher';
-- ;
-- insert into mooc_members
-- select distinct 'mooc_history2' as course_idnumber, member_login, 'student' as member_role
-- from courses_members
-- where course_idnumber rlike '30[23]\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
-- ;






# Отчет по количеству юзеров для всех МООК-ов
select count(*) as 'total members for MOOCs' from mooc_members;




#
# Секция для преподавателей каф. Русского языка (которые заполняют ведомости в курсах иностранных языков на основных факультетах)
#
-- секция была добавлена: 14.10.2024


# 1.	Аверьянова Наталья Анатольевна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Аверьянова Наталья Анатольевна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(ХТ-144|РХТ-148|РХТ-149|РХТ-249|ПП-151|ПП-152|ПП-153|ПП-251|ПП-252)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;


# 2.	Белоус Елена Сергеевна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Белоус Елена Сергеевна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(М-133|М-134|МС-229|МС-230|МВ-231|М-234|АТ-114|АТ-115|АТ-116|АТ-117|РХТ-248)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;


# 3.	Белякова Лариса Фёдоровна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Белякова Лариса Фёдоровна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(ИВТ-161|ИВТ-162|ИВТ-163|ПрИн-166|ПрИн-167|ПрИн-168|ИВТ-261|ИВТ-262|ИВТ-263|ИВТ-264|ПрИн-266|ХТ-242|ХТ-244|БИ-163|СМ-1|МИР-1|УТО-1Н|ЭКОМ-1|ППМ-1|ЛПМ-1н|ТМ-1Н|ВМЦЭ-1|ЭФОР-1|ФМ-1|ПОАС-1.1|ЭВМ-1.1|САПР-1.3|АП-1Н|ТЭРА-1Н)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;


# 4.	Казьмина Ирина Викторовна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Казьмина Ирина Юрьевна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(ЭМ-155/1|Э-158|МС-129|МС-130)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;


# 5.	Карабань Наталья Александровна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Карабань Наталья Александровна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(Э-158/1|Э-159/1|Э-256|Э-257|Э-258/1|Э-259/1)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;


# 6.	Млечко Александр Владимирович

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Млечко Александр Владимирович") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(ХТФ-241|ХТФ-243|АТ-214|АТ-217|ЭП-262)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;

# … Позже – все группы ФПИК и Красноармейского ф-та …


# 7.	Погасий Полина Дмитриевна

insert into mooc_members
select distinct course_idnumber, staff_login as member_login, 'teacher' as member_role
FROM
(select distinct staff_login
from staff_raw_table
where concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = "Погасий Полина Дмитриевна") t
JOIN
(SELECT distinct course_idnumber FROM cohortgroups_to_courses_enrolments
where groupname rlike "^(Э-159|ЭП-161|ЭМ-254|ЭМ-255|Э-258|Э-259|КТО-124|УТС-220|АТП-221|АДП-222|МиР-223|КТО-224)$"
AND course_idnumber rlike '-(000000016|000001199|000003759|000005442|000005472|000005473|000006886)\\*') s
;




#
# Секция для создания индивидуальных курсов для преподавателей каф. Иностранных языков (По умолчанию нужно зачислить на курс только самого преподавателя)
#
-- секция была добавлена: 20.01.2025


-- Новая категория курсов:
-- ИЯ ИНД. КУРСЫ
-- foreign_personal

-- Иностранный язык <Фамилия> Бакалавриат 1курс
-- foreign_bak_<course-N>_<teacher_code>



DROP TABLE IF EXISTS `foreign_lang_teachers_personal`;

CREATE TABLE `foreign_lang_teachers_personal` (
  `faculty` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `edu_level` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'Бакалавриат',
  `course_number` varchar(3) CHARACTER SET utf8mb4 NOT NULL DEFAULT '1',
  `surname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `fio` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `language` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'Иностранный язык'
);

insert into foreign_lang_teachers_personal (faculty, edu_level, course_number, surname, fio, language)
VALUES
('', 'Магистратура','1', 'Топоркова', 'Топоркова Ольга Викторовна', 'Иностранный язык')  ,
('', 'Бакалавриат', '1', 'Ионкина', 'Ионкина Екатерина Юрьевна', 'Иностранный язык')  ,
('', 'Бакалавриат', '1', 'Лихачёва', 'Лихачёва Татьяна Сергеевна', 'Иностранный язык')  ,
('', 'Бакалавриат', '1', 'Стрепетова', 'Стрепетова Надежда Владимировна', 'Иностранный язык')  ,
('', 'Бакалавриат', '1', 'Тисленкова', 'Тисленкова Ирина Александровна', 'Иностранный язык')  ,
('', 'Бакалавриат', '1', 'Чечет', 'Чечет Тамара Ивановна', 'Иностранный язык')
--   ,
-- ('', 'Бакалавриат','1', 'Фамилия', 'Фамилия И О', 'Иностранный язык'),
;
-- ↑ ←  Вписывать новых преподавателей здесь!  ← ↑


-- [1 курс, без деления на факультеты:]
-- Топоркова Ольга Викторовна
-- Ионкина Екатерина Юрьевна
-- Лихачёва Татьяна Сергеевна
-- Стрепетова Надежда Владимировна
-- Тисленкова Ирина Александровна
-- Чечет Тамара Ивановна


-- [ФЭУ] Иностранный язык <Фамилия> Бакалавриат 1курс
-- [ФЭУ] <Фамилия> Иностранный язык Б-1
-- foreign_bak_<course-N>_<teacher_code>

insert into courses_final
select distinct
  concat(
  	f.faculty, IF(f.faculty = '', '', " "),
  	f.language, " ",
  	f.surname, " ",
  	f.edu_level, " ",
  	f.course_number, " курс"
      ) as `fullname`,

  concat(
  	f.faculty, IF(f.faculty = '', '', " "),
  	f.surname, " ",
  	f.`language`, " ",
  	SUBSTRING(f.edu_level, 1, 1), "-",  -- Б / М ← бакалавриат / магистратура
  	f.course_number
      ) as `shortname`,

  "foreign_personal" as `depart_idnumber`,

  concat(
  	"foreign_",
  	(SUBSTRING(f.edu_level, 1, 1)), "-",  -- Б / М ← бакалавриат / магистратура
  	f.course_number, "_",
  	f.faculty, IF(f.faculty = '', '', "_"),
  	st.staff_code
      ) as idnumber

from foreign_lang_teachers_personal f
JOIN staff_raw_table st ON concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = f.fio
;


# Зачисляем преподавателей

insert into mooc_members
select distinct

  concat(
  	"foreign_",
  	(SUBSTRING(f.edu_level, 1, 1)), "-",  -- Б / М ← бакалавриат / магистратура
  	f.course_number, "_",
  	f.faculty, IF(f.faculty = '', '', "_"),
  	st.staff_code
      ) as course_idnumber,

st.staff_login as member_login, 'editingteacher' as member_role
from foreign_lang_teachers_personal f
JOIN staff_raw_table st ON concat(staff_surname, ' ', staff_name, ' ', staff_patronymic) = f.fio
;





# Отчет по количеству юзеров для всех МООК-ов
select count(*) as 'total members for MOOCs + RUS + ENG teachers' from mooc_members;







# Вставка преподавателей и задолжников для МООК в общую таблицу индивидуальных зачислений
# (с отсевом дубликатов)
insert into courses_members (course_idnumber, member_login, member_role)
(
SELECT distinct mm.course_idnumber, mm.member_login, mm.member_role
from mooc_members mm
LEFT JOIN courses_members cm using (course_idnumber, member_login)
WHERE cm.course_idnumber is NULL)
;

# conflicts with deffered cleanup 's INDEX ↓
-- # Вставка преподавателей и задолжников для МООК в общую таблицу индивидуальных зачислений
-- # (с отсевом дубликатов)
-- insert into courses_members (course_idnumber, member_login, member_role)
-- (
-- SELECT distinct course_idnumber, member_login, member_role
-- from mooc_members mm
-- LEFT JOIN courses_members cm using (course_idnumber, member_login, member_role)
-- WHERE cm.course_idnumber is NULL)
-- ;

DROP TABLE `mooc_members`;








#               •○•○•○•○•○•               #

# Аналогично — с группами ...
# cohortgroups_to_courses_enrolments

DROP TABLE IF EXISTS `mooc_cohorts`;

CREATE TABLE IF NOT EXISTS `mooc_cohorts` (
  `groupname` varchar(255) NOT NULL,
  `group_cohort_idnumber` varchar(255) NOT NULL,
  `course_idnumber` varchar(99) NOT NULL DEFAULT '',
  `member_role` varchar(31) NOT NULL DEFAULT ''
);


-- дисциплина ОРГ: '7832     '

# ВолГТУ → МООК ОРГ (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_org1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-7832     \\*1' AND member_role = 'student';
;

# ИАиС → МООК ОРГ (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_org1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[89]|11[05])\\+.*-7832     \\*1' AND member_role = 'student';
;

# ВПИ → МООК ОРГ (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_org1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-7832     \\*1' AND member_role = 'student';
;

# КТИ → МООК ОРГ (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_org1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-7832     \\*1' AND member_role = 'student';
;




-- дисциплина Физика: '(000000008|000000943)'

# ВолГТУ → МООК Физика (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_physics1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
;

# ИАиС → МООК Физика (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_physics1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
;

-- # ВПИ → МООК Физика (1сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_physics1' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
-- ;

-- # КТИ → МООК Физика (1сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_physics1' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000008|000000943)\\*[1-4]' AND member_role = 'student';
-- ;





-- дисциплина История: '000000005'
-- "История": '000000005'
-- "История России": '000003803'
-- "История (История России, всеобщая история)": '000005325'
-- (000000005|000003803|000005325)

# ВолГТУ → МООК История (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_history1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
;

# ИАиС → МООК История (1сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_history1' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
;

-- # ВПИ → МООК История (1сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_history1' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
-- ;

-- # КТИ → МООК История (1сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_history1' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*[12]' AND member_role = 'student';
-- ;




# ВолГТУ → МООК История (2сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_history2' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
;

# ИАиС → МООК История (2сем)
insert into mooc_cohorts
select distinct groupname, group_cohort_idnumber, 'mooc_history2' as course_idnumber, 'student' as member_role
from cohortgroups_to_courses_enrolments
where course_idnumber rlike '(10[89]|11[05])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
;

-- # ВПИ → МООК История (2сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_history2' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
-- ;

-- # КТИ → МООК История (2сем)
-- insert into mooc_cohorts
-- select distinct groupname, group_cohort_idnumber, 'mooc_history2' as course_idnumber, 'student' as member_role
-- from cohortgroups_to_courses_enrolments
-- where course_idnumber rlike '(10[1-7]|11[37])\\+.*-(000000005|000003803|000005325)\\*2' AND member_role = 'student';
-- ;








# Отчет по количеству групп для всех МООК-ов
select count(*) as 'total cohorts for MOOCs' from mooc_cohorts;


# Вставка групп для МООК в общую таблицу зачислений группами
# (с отсевом дубликатов)
insert into cohortgroups_to_courses_enrolments (groupname, group_cohort_idnumber, course_idnumber, member_role)
(
SELECT distinct groupname, group_cohort_idnumber, course_idnumber, member_role
from mooc_cohorts mm
LEFT JOIN cohortgroups_to_courses_enrolments cm using (groupname, group_cohort_idnumber, course_idnumber, member_role)
WHERE cm.course_idnumber is NULL)
;

DROP TABLE `mooc_cohorts`;




# Обновить таблицы с отсроченным удалением строк, с учётом сделанных только что обновлений.

# Приписки групп — 5 дн.
CALL handle_deferred_cleanup_for_table('cohortgroups_to_courses_enrolments', 'group_cohort_idnumber, course_idnumber', 5 * 24 * 3600);


# Участники курсов (помимо групп) — 10 дн.
CALL handle_deferred_cleanup_for_table('courses_members', 'course_idnumber, member_login', 10 * 24 * 3600);
